import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image, TextInput,FlatList,Dimensions,TouchableOpacity } from 'react-native'
import { useFonts } from 'expo-font';
import { Icon, Button } from 'react-native-elements'
import axios from 'axios'

export default function Home() {    
    const [items, setItems]=useState([]);

    let [fontsLoaded] = useFonts({
        'ReemKufi': require('../assets/fonts/ReemKufi-Regular.ttf'),
    });

    var path = "assets/images/1.jpg";

    const GetData = () => {
        axios.get('https://615404182473940017efab41.mockapi.io/items')
        .then(res=>{
            const data1 = (res.data)
            setItems(data1)            
        })

        // console.log(eval("require('../assets/images/1.jpg')"));
    }

    useEffect(()=>{
        GetData()
    },[])

    return (
        <View style={styles.container}>
            <View style={styles.bar}>
                <Icon style={{margin:10}} size={30} name='menu' type='ionicon' color='white'/>
                <TextInput  style = {styles.textInput} placeholder="Search"  value="" onChangeText={(value)=>setPassword(value)}/>
                <Icon style={{margin:10}} name='search' type='ionicon' color='white'/>
            </View>
            <View style={{margin:10,paddingBottom: 60}}>            
                <FlatList
                        style={{width:'100%'}}
                        numColumns={2}
                        data={items}
                        keyExtractor={(item,index)=>`${item.id}-${index}`}
                        renderItem={({item})=>{
                            return (
                            <View style={{flex:1}}>
                                <TouchableOpacity style={styles.row}>
                                    <View style={{padding:5}}>
                                        <Image style={{width:'100%',height:150,alignSelf:'center',margin:5}} source={require(`../${item.image}`)}/>
                                        <Text style={{margin:5,fontFamily:'ReemKufi',fontWeight:'bold',alignSelf:"center"}}>{item.title}</Text>
                                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>                                
                                            <Button buttonStyle={{marginRight:15,backgroundColor:'#495B6C', height:25}} titleStyle={{fontFamily:'ReemKufi',fontSize:13}} title="Detail"/>                                            
                                            <Icon size={30} name='cart-outline' type='ionicon'/>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>    
                            )
                        }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'#F4F9FE',
    },
    bar:{
        flexDirection:"row",
        justifyContent:"space-between",
        width:'100%',
        height:50,
        backgroundColor:'#495B6C'
    },
    image:{
        width:'100%',
        height:360,
        alignSelf:'center'
    },
    textInput:{
        borderBottomWidth: 1,
        marginLeft:5,
        marginRight:5,
        marginBottom:10,
        fontFamily:'ReemKufi',
        width:250,
        borderBottomColor:"#FFFFFF",
        placeholderTextColor: "#FFFFFF"
    }, 
    row:{
        borderColor:'#495B6C',
        borderWidth:1,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 15,
        borderRadius:15,
        height: Dimensions.get('window').width
    },   
})
