import React from 'react'
import { StyleSheet, View, ImageBackground } from 'react-native'

const bg = require("../assets/images/splash.png");

export default function Splashscreen() {
    return (
        <View style={styles.container}>
            <ImageBackground source={bg} resizeMode="cover" style={styles.background}>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    background: {
        flex: 1,
        justifyContent: "center"
    },
})
