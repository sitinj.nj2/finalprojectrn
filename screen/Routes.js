import React from 'react'
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import 'react-native-gesture-handler';
import Signin from './Signin';
import Signup from './Signup';
import Splashscreen from './Splash';
import Welcome from './Welcome';
import Account from './Account';
import Detail from './Detail';
import Home from './Home';

const Stack = createNativeStackNavigator();

export default function Routes() {
    return (
        <NavigationContainer>
        <Stack.Navigator initialRouteName="Welcome" >
          <Stack.Screen name='Welcome' component={Welcome} options={{ headerShown: false }} />
          <Stack.Screen name='Signin' component={Signin} options={{ headerShown: false }} />
          <Stack.Screen name='Signup' component={Signup} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={Home}  options={{ headerShown: false }}/>
          <Stack.Screen name='Detail' component={Detail}  options={{ headerShown: false }}/>
          <Stack.Screen name='Account' component={Account}  options={{ headerShown: false }}/>
        </Stack.Navigator>
        </NavigationContainer>
    )
}
