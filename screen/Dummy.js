const Dummy =[
    {
        id      : "1",
        harga   : "15000",
        image   : require("../assets/images/1.jpg"),
        title   : "Mini Bouquet",
        desc    : "Buket dengan satu tangkai",
        type    : "bouquet"
    },
    {
        id      : "2",
        harga   : "100000",
        image   : require("../assets/images/2.jpg"),
        title   : "Men Bouquet",
        desc    : "Buket isi kopi dan rokok",
        type    : "custom"
    },
    {
        id      : "3",
        harga   : "75000",
        image   : require("../assets/images/3.jpg"),
        title   : "Cook Bouquet",
        desc    : "Buket isi bumbu masakan",
        type    : "custom"
    },
    {
        id      : "4",
        harga   : "90000",
        image   : require("../assets/images/4.jpg"),
        title   : "Snack Bouquet",
        desc    : "Buket isi snack dan coklat",
        type    : "custom"
    },
]

export {Dummy};