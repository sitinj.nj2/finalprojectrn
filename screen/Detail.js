import React from 'react'
import { StyleSheet, Text, View , Image, TouchableHighlight } from 'react-native'
import { useFonts } from 'expo-font';
import { Icon,Divider  } from 'react-native-elements'
import { color } from 'react-native-elements/dist/helpers';

const bg = require("../assets/images/bg.png");

export default function Detail() {
    let [fontsLoaded] = useFonts({
        'ReemKufi': require('../assets/fonts/ReemKufi-Regular.ttf'),
    });

    return (
        <View style={styles.container}>
            <View style={styles.bar}>
                <Icon style={{margin:10}} name='arrow-back-outline' type='ionicon' color='white'/>
                <Text style={styles.title}>Mini Bouquet</Text>
            </View>
            <View>
                <Image style={styles.image} source={require("../assets/images/1.jpg")}/>
            </View>
            <Divider orientation="vertical" width={10} />
            <View style={{margin:15,flexDirection:'row'}}>
                <Text style={styles.text}>Color</Text>
                <Icon name='ellipse' style={{marginRight:10}} size={25} type='ionicon' color='#FFB6C1'/>
                <Icon name='ellipse' style={{marginRight:10}} size={25} type='ionicon' color='#000000'/>
                <Icon name='ellipse' style={{marginRight:10}} size={25} type='ionicon' color='#87CEFA'/>
                <Icon name='ellipse' style={{marginRight:10}} size={25} type='ionicon' color='#FAEBD7'/>
            </View>
            <Divider orientation="vertical" width={10} />
            <View style={{marginLeft:15, marginRight:15, marginTop:10}}>
                <Text style={styles.text}>Deskripsi</Text>
            </View>
            <View style={{marginLeft:15, marginRight:15, marginTop:10}}>
                <Text style={styles.text2}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
            </View>
            <View style={styles.foot}>
                <Icon size={35} name='cart-outline' type='ionicon'/>
                <TouchableHighlight
                    style={styles.button}
                    onPress={() => this.submitSuggestion(this.props)}
                    underlayColor='#fff'>
                    <Text style={styles.buttonText}>Add to Basket</Text>
                </TouchableHighlight>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'#F4F9FE',
    },
    bar:{
        flexDirection:"row",
        justifyContent:'space-between',
        width:'100%',
        height:50,
        backgroundColor:'#495B6C'
    },
    title:{
        fontSize:20,
        color:'white',
        margin:10,
        fontFamily:'ReemKufi'
    },
    image:{
        width:250,
        height:300,
        margin:5,
        alignSelf:'center'
    },
    text:{
        fontFamily:'ReemKufi',
        color:'#495B6C',
        fontSize:15,
        fontWeight:'bold',
        marginRight:35
    },
    text2:{
        fontFamily:'ReemKufi',
        color:'#495B6C',
        fontSize:13,
    },
    foot:{
        marginTop:10,
        marginLeft:15,
        marginRight:15,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    button:{
        width:200,
        padding:10,
        backgroundColor: '#495B6C',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
    },
    buttonText: {
        fontSize: 17,
        color: '#fff',
        textAlign: 'center',
        fontFamily:'ReemKufi',
    }
})
