import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { StyleSheet, ImageBackground, View,Text,TextInput,Alert} from 'react-native';
import { useFonts } from 'expo-font';
import firebase from 'firebase/app';

// const firebaseConfig = {
//     apiKey: "AIzaSyDPfJZKg1HW6YC6_xv4rKN866Vhx-b5wok",  
//     authDomain: "authfinalprojectrn.firebaseapp.com",  
//     projectId: "authfinalprojectrn",  
//     storageBucket: "authfinalprojectrn.appspot.com",  
//     messagingSenderId: "758356829076",  
//     appId: "1:758356829076:web:f506e3ceaa8e29cab2e581"  
// };

// if(!firebase.apps.length){
//     firebase.initializeApp(firebaseConfig)
// }

const bg = require("../assets/images/bg.png");

export default function Signup({ navigation }) {
    const [fullname, setFullname] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [password, setPassword] = useState("");
    
    const [isError, setIsError] = useState(false);

    let [fontsLoaded] = useFonts({
        'ReemKufi': require('../assets/fonts/ReemKufi-Regular.ttf'),
    });

    const submit = () => {
        if(email=="" || fullname=="" || phone=="" || password==""){
            // Alert.alert('Error','Form tidak boleh kosong')
        }else{
            firebase.auth().createUserWithEmailAndPassword(email,password)
            .then((res)=>{
                console.log(res)
                Alert.alert('Success','Signup Berhasil');
            }).catch((err)=>{
                Alert.alert('Error',err);
            })
            // const data={
            //     fullname,email,phone,password
            // }
    
            // axios.post('https://615404182473940017efab41.mockapi.io/users', data)
            //     .then(res=>{
            //         console.log('res :', res);
            //         setFullname("")
            //         setEmail("")
            //         setPhone("")
            //         setPassword("")
            //         successAlert()
            // }).catch(err=>{
            //     setIsError(true)
            //     console.log('error :', err);
            // })
        }

        
    }

    const successAlert = () =>
        Alert.alert(
        "Success",
        "Register User Success",
        [{ text: "OK", onPress: () => this.hi}],
        { cancelable: false }
    );

    const errorAlert = () =>
        Alert.alert(
        "Success",
        "Register User Success",
        [{ text: "OK", onPress: () => this.hi}],
        { cancelable: false }
    );

    return (
        <View style={styles.container}>
            <ImageBackground source={bg} resizeMode="stretch" style={styles.background}>
                <View style={{flex:1, justifyContent:'space-between'}}>                    
                </View>
                <View style={styles.box}>
                    <Text style={styles.text}>Sign Up</Text>
                    <TextInput  style = {styles.textInput} placeholder="Name"  value={fullname} onChangeText={(value)=>setFullname(value)}/>
                    <TextInput  style = {styles.textInput} placeholder="Email"  value={email} onChangeText={(value)=>setEmail(value)}/>
                    <TextInput  style = {styles.textInput} placeholder="Phone Number"  value={phone} onChangeText={(value)=>setPhone(value)}/>
                    <TextInput  style = {styles.textInput} placeholder="Password"  value={password} onChangeText={(value)=>setPassword(value)}/>
                    <View style={{flexDirection:'row',marginLeft:15,marginRight:20,marginTop:20,justifyContent:'space-between'}}>
                        <Text style={{fontSize:'18',color:'#495B6C', fontFamily:'ReemKufi'}}>Already have an account ?</Text>
                        <Text style={{fontSize:'18',color:'#677E95', fontFamily:'ReemKufi'}} onPress={submit}>Sign Up</Text>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column'
    },
    background: {
        flex: 1,
        justifyContent: "center",
        width: '100%', 
        height: 525
    },
    text: {
        color: "#495B6C",
        fontSize: 30,
        fontFamily:'ReemKufi',
        textAlign: "left",
        marginTop: 15,
        marginLeft:15
    },
    textInput:{
        borderBottomWidth: 1,
        marginRight:20,
        marginLeft:15,
        marginBottom: 5,
        fontFamily:'ReemKufi',
        padding:10,
        borderBottomColor:"#819DB8",
        placeholderTextColor: "#819DB8"
    },
    box:{
        flex:1,
        borderRadius:15,
        flexDirection:'column',
        backgroundColor:'#F4F9FE',
    },
})

