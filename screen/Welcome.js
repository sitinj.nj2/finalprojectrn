import React from 'react'
import { StyleSheet, ImageBackground, View,Text, Button,TouchableHighlight } from 'react-native'
import { useFonts } from 'expo-font';

const bg = require("../assets/images/bg.png");

export default function Welcome({navigation}) {
    let [fontsLoaded] = useFonts({
        'ReemKufi': require('../assets/fonts/ReemKufi-Regular.ttf'),
    });

    return (
        <View style={styles.container}>
            <ImageBackground source={bg} resizeMode="stretch" style={styles.background}>
                <View style={{flex:4, justifyContent:'space-between'}}>
                    <Text  style={styles.text}>WELCOME</Text>
                </View>
                <View style={styles.box}>
                    <TouchableHighlight
                        style={styles.button1}
                        onPress={()=>navigation.navigate('Signup', {
                            screen: 'Signup',
                        })}
                        underlayColor='#fff'>
                        <Text style={styles.buttonText1}>Sign Up</Text>
                    </TouchableHighlight>
                    <TouchableHighlight
                        style={styles.button2}
                        onPress={()=>navigation.navigate('Signin', {
                            screen: 'Signin',
                        })}
                        underlayColor='#fff'>
                        <Text style={styles.buttonText2}>Sign In</Text>
                    </TouchableHighlight>
                </View>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column'
    },
    background: {
        flex: 1,
        justifyContent: "center",
        width: '100%', 
        height: 525
    },
    text: {
        color: "white",
        fontSize: 30,
        lineHeight: 84,
        fontFamily:'ReemKufi',
        textAlign: "center",
        marginTop:250,
        justifyContent:"space-between"
    },
    box:{
        flex:1,
        borderRadius:15,
        flexDirection:'column',
        backgroundColor:'#F4F9FE',
    },
    button1:{
        marginRight: 40,
        marginLeft: 40,
        marginTop: 15,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: '#495B6C',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
    },
    button2:{
        marginRight: 40,
        marginLeft: 40,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#495B6C',
    },
    buttonText1: {
        fontSize: 17,
        color: '#fff',
        textAlign: 'center',
        fontFamily:'ReemKufi',
    },
    buttonText2: {
        fontSize: 17,
        color: '#495B6C',
        textAlign: 'center',
        fontFamily:'ReemKufi',
    }
})