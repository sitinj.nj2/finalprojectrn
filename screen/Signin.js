import React from 'react'
import { StyleSheet, ImageBackground, View,Text,TextInput,} from 'react-native'
import { useFonts } from 'expo-font';

const bg = require("../assets/images/bg.png");

export default function Signin() {
    let [fontsLoaded] = useFonts({
        'ReemKufi': require('../assets/fonts/ReemKufi-Regular.ttf'),
    });

    return (
        <View style={styles.container}>
            <ImageBackground source={bg} resizeMode="stretch" style={styles.background}>
                <View style={{flex:2, justifyContent:'space-between'}}>                    
                </View>
                <View style={styles.box}>
                    <Text style={styles.text}>Sign In</Text>
                    <TextInput  style = {styles.textInput} placeholder="Email"  value="" onChangeText={(value)=>setPassword(value)}/>
                    <TextInput  style = {styles.textInput} placeholder="Password"  value="" onChangeText={(value)=>setPassword(value)}/>
                    <View style={{flexDirection:'row',marginLeft:15,marginRight:20,marginTop:20,justifyContent:'space-between'}}>
                        <Text style={{fontSize:'18',color:'#495B6C', fontFamily:'ReemKufi'}}>Forgot password ?</Text>
                        <Text style={{fontSize:'18',color:'#677E95', fontFamily:'ReemKufi'}}>Sign In</Text>
                    </View>
                </View>
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column'
    },
    background: {
        flex: 1,
        justifyContent: "center",
        width: '100%', 
        height: 525
    },
    text: {
        color: "#495B6C",
        fontSize: 30,
        fontFamily:'ReemKufi',
        textAlign: "left",
        marginTop: 15,
        marginLeft:15
    },
    textInput:{
        borderBottomWidth: 1,
        marginRight:20,
        marginLeft:15,
        marginBottom: 5,
        fontFamily:'ReemKufi',
        padding:10,
        borderBottomColor:"#819DB8",
        placeholderTextColor: "#819DB8"
    },
    box:{
        flex:1,
        borderRadius:15,
        flexDirection:'column',
        backgroundColor:'#F4F9FE',
    },
})