import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { useFonts } from 'expo-font';
import { Icon } from 'react-native-elements'

const bg = require("../assets/images/bg.png");

export default function Account() {
    let [fontsLoaded] = useFonts({
        'ReemKufi': require('../assets/fonts/ReemKufi-Regular.ttf'),
    });

    return (
        <View style={styles.container}>
            <View style={styles.bar}>
                <Icon style={{margin:10}} name='arrow-back-outline' type='ionicon' color='white'/>
                <Text style={styles.title}>My Account</Text>
            </View>
            <View>
                <Image style={styles.image} source={require("../assets/images/profpict.JPG")}/>
            </View>
            <View style={styles.info}>
                <Text style={styles.bio}>Nama</Text>
                <Text style={styles.bio2}>Siti Nurjanah</Text>
                <Text style={styles.bio}>Email</Text>
                <Text style={styles.bio2}>sitinj.nj2@gmailo.com</Text>
                <Text style={styles.bio}>Phone Number</Text>
                <Text style={styles.bio2}>0822946348</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
        backgroundColor:'#F4F9FE',
    },
    bar:{
        flexDirection:"row",
        justifyContent:'space-between',
        width:'100%',
        height:50,
        backgroundColor:'#495B6C'
    },
    title:{
        fontSize:20,
        color:'white',
        margin:10,
        fontFamily:'ReemKufi'
    },
    image:{
        width:'100%',
        height:360,
        alignSelf:'center'
    },
    info:{
        flexDirection:'column',
        margin:20
    },
    bio:{
        fontFamily:'ReemKufi',
        fontSize:17,
        color:'#30404F',
        marginBottom:5,
        fontWeight:'bold'
    },
    bio2:{
        fontFamily:'ReemKufi',
        fontSize:15,
        color:'#677E95',
        marginBottom:15,
    }
})
